package org.scenariotools.bpk.examples

import kotlin.system.*

import org.scenariotools.bpk.*

fun main (args:Array<String>) {

    val timeElapsed = measureTimeMillis {
        bProgram.run()
    }
    println("timeElapsed: $timeElapsed")

}

val addColdThreeTimes = bthread("AddColdThreeTimes")  {
    addBThread("AddColdThreeTimes"){
        repeat (3){
            request(addCold)
        }
    } // example of how to add a BThread via another BThread
    repeat (3){
        request(addHot)
    }
}

val interleave = bthread ("Interleave") {
    repeat (3){
        waitForAndBlock(addCold, addHot)
        waitForAndBlock(addHot, addCold)
    }
}

val bProgram = VerboseBProgram(addColdThreeTimes, interleave)


class VerboseBProgram(vararg initialBThreads: NameToBThreadMain) : BProgram(*initialBThreads){
	override fun eventSelected(selectedEvent : Event){
        println("EXECUTED $selectedEvent")
	}
}

data class TapEvent(val name:String) : Event() {
	override fun toString() : String = name
}

val addHot = TapEvent("AddHot")
val addCold = TapEvent("AddCold")

