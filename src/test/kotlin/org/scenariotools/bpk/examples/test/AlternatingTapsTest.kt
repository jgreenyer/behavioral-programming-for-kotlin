package org.scenariotools.bpk.examples.test

import org.junit.jupiter.api.*

import org.scenariotools.bpk.*
import org.scenariotools.bpk.examples.*

var testSuccessful : Boolean = false

class AlternatingTabsTest{


	@Test
	fun test() {
		val bProgram = VerboseBProgram(addColdThreeTimes, interleave, testAlternatingTapsOccurred)

	    bProgram.run()
		assert(testSuccessful)
	}

	val testAlternatingTapsOccurred = bthread("testAlternatingTapsOccurred"){

		mutableSetOf<Event>()

		val firstEvent = bSync(NOEVENTS, ConcreteEventSet(addHot, addCold), NOEVENTS)
		val secondEvent =
				if (firstEvent == addHot){
					addCold
				}else{
					addHot
				}

		bSync(NOEVENTS, secondEvent, NOEVENTS)

		repeat (2){
			waitFor(firstEvent)
			waitFor(secondEvent)
		}

		testSuccessful = true

	}

}

