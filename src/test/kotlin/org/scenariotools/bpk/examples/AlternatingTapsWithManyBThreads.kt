package org.scenariotools.bpk.examples

import kotlinx.coroutines.runBlocking

import kotlin.system.*

import org.scenariotools.bpk.*

fun main (args:Array<String>) = runBlocking {
	
	val numberOfBThreads = 1000

	val setOfBThreads : MutableSet<NameToBThreadMain> = mutableSetOf()
	
	for (i in 1..numberOfBThreads) {
		setOfBThreads.add(bthread("AddHotThreeTimes") {
			addBThread("AddColdThreeTimes"){
				val addCold = NumberedTapEvent("AddCold", i)
				repeat (1){
					request(addCold)
				}
			} // example of how to add a BThread via another BThread
			val addHot = NumberedTapEvent("AddHot", i)
			repeat (1){
				request(addHot)
			}
		})
	}
	
	setOfBThreads.add(bthread("Interleave") {

		val blockedEvents = HashSet<Event>()

		for (i in 1..numberOfBThreads) {
			val addHot = NumberedTapEvent("AddHot", i)
			val addCold = NumberedTapEvent("AddCold", i)
			blockedEvents.add(addHot)
			blockedEvents.add(addCold)
		}

		val blockedEventSet = MutableConcreteEventSet(blockedEvents)


		for (i in 1..numberOfBThreads) {
			val addHot = NumberedTapEvent("AddHot", i)
			val addCold = NumberedTapEvent("AddCold", i)

			// there should be a nicer way to do this...
			val blockedEventsRemovedCold = MutableConcreteEventSet(blockedEventSet)
			blockedEventsRemovedCold.remove(addCold)
			val blockedEventsRemovedHot = MutableConcreteEventSet(blockedEventSet)
			blockedEventsRemovedHot.remove(addHot)

			waitForAndBlock(addCold, blockedEventsRemovedCold)
			waitForAndBlock(addHot, blockedEventsRemovedHot)
		}
	})
	
    //val bProgram = BProgram(setOfBThreads)
    val bProgram = VerboseBProgram(*setOfBThreads.toList().toTypedArray())
	
	val timeElapsed = measureTimeMillis {
		bProgram.run()
	}
	println("$timeElapsed")	

}

data class NumberedTapEvent(val name : String, val i : Int) : Event()
