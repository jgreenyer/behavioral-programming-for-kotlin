package org.scenariotools.iosmk.examples.pingpong

import org.scenariotools.iosmk.*


fun main (args:Array<String>) {

    val scenarioProgram = ScenarioProgram()
    scenarioProgram.addEnvironmentClass(Env::class)
    scenarioProgram.addGuaranteeScenario("TestDriverScenario"){
        println("Starting $name")
        println("requesting start")
        request(MessageEvent(Env, PongMe, PongMe::start))
    }
    scenarioProgram.addGuaranteeScenario("Ping", MessageEvent(Env, PongMe, PongMe::start)) {
        println("$name requesting ping(1) ** starting :)")
        request(MessageEvent(PongMe, PingMe, PingMe::ping, 1))

        interruptingEvents.add(SymbolicMessageEvent(PingMe, PongMe, PongMe::pong, 101..200, ANY))

        while (true){
            waitFor(SymbolicMessageEvent(PingMe, PongMe, PongMe::pong, 1..100, ANY))
            val i = ((lastMessage()!!.parameters[0] as Int) + (lastMessage()!!.parameters[1]) as Int)
            println("$name requesting ping($i)")
            request(MessageEvent(PongMe, PingMe, PingMe::ping, i))
        }
    }
    scenarioProgram.addGuaranteeScenario("Pong",SymbolicMessageEvent(PongMe, PingMe, PingMe::ping, 1..200)){
        val i = (lastMessage()!!.parameters[0] as Int)
        println("$name requesting pong($i, ${i+1})")
        request(MessageEvent(PingMe, PongMe, PongMe::pong, i, i+1))
        println("$name terminating")
    }

    scenarioProgram.run()

}