package org.scenariotools.iosmk.examples.productioncell

import org.scenariotools.bpk.NOEVENTS
import org.scenariotools.iosmk.ScenarioProgram

fun main (args:Array<String>) {

    val scenarioProgram = ScenarioProgram()
    scenarioProgram.addEnvironmentClass(TableSensor::class)

    scenarioProgram.addAssumptionScenario("ArmAArrivesAtPress", moveToPress){

        bSuspend(2000, NOEVENTS, "ArmA moving to press")

        println("$name requesting arrivedAtPress")
        request(arrivedAtPress)
        println("$name terminating")

    }
    scenarioProgram.addAssumptionScenario("ArmAArrivesAtTable", moveToTable){

        bSuspend(2000, NOEVENTS,"ArmA moving to table")

        println("$name requesting arrivedAtTable")
        request(arrivedAtTable)
        println("$name terminating")
    }

    scenarioProgram.addGuaranteeScenario("PickUpOnBlankArrived", blankArrived) {

        println("$name requesting pickUp")
        request(pickUp)
        println("$name requesting moveToPress")
        request(moveToPress)
        println("$name terminating")
    }

    scenarioProgram.addGuaranteeScenario("ReleaseAtPress", arrivedAtPress) {

        println("$name requesting release")
        request(release)
        println("$name requesting moveToTable")
        request(moveToTable)
        println("$name terminating")
    }

    scenarioProgram.addGuaranteeScenario("TestDriverScenario") {
        println("Starting $name")
        println("requesting blankArrived")
        request(blankArrived)

        bSuspend(5000, NOEVENTS,"Next blank approaching")

        println("requesting blankArrived")
        request(blankArrived)
    }


    scenarioProgram.run()

//    val initializerBThread = InitializerBThread(PingScenario::class)
//
//    val bProgram = BProgram(TestDriverScenario(), initializerBThread)
//    bProgram.run()

}