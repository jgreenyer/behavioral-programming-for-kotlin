package org.scenariotools.iosmk.examples.rovercontroller.guarantees

import org.scenariotools.iosmk.*
import org.scenariotools.iosmk.examples.rovercontroller.*

val predictLeaderPos = scenario("PredictLeaderPos", SymbolicMessageEvent(Sensors,PosPredictor, PosPredictor::telemetries, ANY)){
    val t1 = lastMessage()!!.parameters[0] as Telemetries
    waitFor(Sensors to PosPredictor send PosPredictor::telemetries param ANY)
    val t2 = lastMessage()!!.parameters[0] as Telemetries
    request(PosPredictor to FollowerCtrl send FollowerCtrl::predictedTelemetries param predictTelemetries(t1,t2))
}

private val cyclesToPredictAhead = 5

private fun predictTelemetries(rt1 : Telemetries, rt2 : Telemetries) : Telemetries{
    val predictedLeaderPos = predictPos(rt1.leaderPos, rt2.leaderPos)
    val predictedFollowerPos = predictPos(rt1.followerPos, rt2.followerPos)
    val predictedDistance = distance(predictedFollowerPos, predictedLeaderPos)

//        println("actual distance   : " +  rt2.distance)
//        println("predicted distance: " +  predictedDistance)

    return Telemetries(predictedLeaderPos,predictedFollowerPos,predictedDistance, rt2.followerCompass)
}

private fun predictPos(pos1 : Coordinate, pos2 : Coordinate) : Coordinate {
//        println("pos1             : " +  pos1)
//        println("pos2             : " +   pos2)
    val longOffset = pos2.long - pos1.long
    val latOffset = pos2.lat - pos1.lat
    val returnCoordinate = Coordinate(pos2.long+longOffset*cyclesToPredictAhead, pos2.lat+latOffset*cyclesToPredictAhead)
//        println("returnCoordinate : " +  returnCoordinate)
    return returnCoordinate
}

private fun distance (pos1 : Coordinate, pos2 : Coordinate) : Double {
    val longOffset = pos2.long - pos1.long
    val latOffset = pos2.lat - pos1.lat
    return Math.sqrt(longOffset*longOffset + latOffset*latOffset)
}
