package org.scenariotools.iosmk.examples.rovercontroller

data class Telemetries(
        val leaderPos : Coordinate,
        val followerPos : Coordinate,
        val distance: Double,
        val followerCompass : Double
)

data class Coordinate(val long : Double, val lat : Double)