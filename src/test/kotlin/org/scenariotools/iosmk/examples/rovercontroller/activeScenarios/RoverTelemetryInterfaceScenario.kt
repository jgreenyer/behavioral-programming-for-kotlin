package org.scenariotools.iosmk.examples.rovercontroller.activeScenarios

import kotlinx.coroutines.delay
import org.scenariotools.iosmk.scenario
import org.scenariotools.iosmk.examples.rovercontroller.*
import org.scenariotools.iosmk.param
import org.scenariotools.iosmk.send

val roverTelemetryInterfaceScenario = scenario("RoverTelemetryInterfaceScenario") {

    println("Starting $name")

    println("program   : " + ref.send("ready"))

    while(true){
        try {
            bSuspend()
            delay(250)
            val leaderPos = parseCoordinate(ref.send("Leader,GPS()"))
            val followerPos = parseCoordinate(rover.send("Rover,GPS()"))
            val followerCompass = parseCompass(rover.send("Rover,getCompass()"))
            val distance = parseDistance(ref.send("Leader,Distance()"))
            val rt = Telemetries(leaderPos, followerPos, distance, followerCompass)
            bResume()
            request(Sensors to PosPredictor send PosPredictor::telemetries param rt)
        } catch (e : Exception){
            println("Exception : $e")
            terminate()
        }
    }
}


private fun parseCoordinate(message: String?): Coordinate {
    //println("parseCoordinate : $message")
    if (message == null) throw IllegalArgumentException()
    val posFirstComma = message.indexOf(",")
    val posSecondComma = message.indexOf(",", posFirstComma+1)
    val long = message.substring(posFirstComma+1, posSecondComma)
    val lat = message.substring(posSecondComma+1, message.length-1)
    val coordinate = Coordinate(long.toDouble(), lat.toDouble())
    return coordinate
}

private fun parseDistance(message: String?): Double {
    //println("parseDistance : $message")
    if (message == null) throw IllegalArgumentException()
    val posFirstComma = message.indexOf(",")
    val distance = message.substring(posFirstComma+1, message.length-1)
    return distance.toDouble()
}

private fun parseCompass(message: String?): Double {
    //println("parseCompass : $message")
    if (message == null) throw IllegalArgumentException()
    val posFirstComma = message.indexOf(",")
    val distance = message.substring(posFirstComma+1, message.length-1)
    return distance.toDouble()
}
