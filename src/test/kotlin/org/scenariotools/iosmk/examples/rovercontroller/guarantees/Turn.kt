package org.scenariotools.iosmk.examples.rovercontroller.guarantees

import org.scenariotools.iosmk.*
import org.scenariotools.iosmk.examples.rovercontroller.*

val turn = scenario("Turn", SymbolicMessageEvent(PosPredictor,FollowerCtrl, FollowerCtrl::predictedTelemetries, ANY)) {
    val t = (lastEvent as MessageEvent<*>).parameters[0] as Telemetries
    val relativeAngle = relativeAngle(t)

    //println("relativeAngle: " + relativeAngle)

    blockedEvents.add(SymbolicMessageEvent(FollowerCtrl, DriveCmd, DriveCmd::turn, ANY))
    waitFor(FollowerCtrl to DriveCmd send DriveCmd::move param ANY)
    blockedEvents.clear()
    interruptingEvents

    if (relativeAngle > 10 && relativeAngle < 180 ) // turn left
        request(FollowerCtrl to DriveCmd send DriveCmd::turn param TurnKind.LEFT)
    else if (relativeAngle > 180 && relativeAngle < 350 ) // turn right
        request(FollowerCtrl to DriveCmd send DriveCmd::turn param TurnKind.RIGHT)
    else
        request(FollowerCtrl to DriveCmd send DriveCmd::turn param TurnKind.NONE)

}

private fun relativeAngle(rt: Telemetries) = ((((rt.followerCompass) - angle(rt)) + 360) % 360)
private fun angle(rt: Telemetries) = angle(rt.followerPos, rt.leaderPos)
private fun angle(c1 : Coordinate, c2 : Coordinate) = angle(c1.long, c1.lat, c2.long, c2.lat)
private fun angle(x1 : Double, y1 : Double, x2 : Double, y2 : Double) = Math.toDegrees(Math.atan2(x2 - x1, y2 - y1))

enum class TurnKind {
    LEFT, RIGHT, NONE
}