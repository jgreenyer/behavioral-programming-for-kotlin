package org.scenariotools.iosmk.examples.rovercontroller.guarantees

import org.scenariotools.iosmk.*
import org.scenariotools.iosmk.examples.rovercontroller.*

val move = scenario("Move", FollowerCtrl::predictedTelemetries.symbolicEvent()) {
    val t = lastMessage()!!.parameters[0] as Telemetries

    if(t.distance > 13){
        request(FollowerCtrl to DriveCmd send DriveCmd::move param MoveKind.FWD)
        request(FollowerCtrl to DriveCmd send DriveCmd::move param MoveKind.FWD)
    } else if(t.distance < 12){
        request(FollowerCtrl to DriveCmd send DriveCmd::move param MoveKind.BWD)
    } else if(t.distance <= 13){
        request(FollowerCtrl to DriveCmd send DriveCmd::move param MoveKind.STOP)
    }
}

enum class MoveKind {
    FWD, STOP, BWD
}