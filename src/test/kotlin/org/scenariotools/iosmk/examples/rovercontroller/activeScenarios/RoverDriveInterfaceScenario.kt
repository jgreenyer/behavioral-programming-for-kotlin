package org.scenariotools.iosmk.examples.rovercontroller.activeScenarios

import org.scenariotools.iosmk.*
import org.scenariotools.iosmk.examples.rovercontroller.DriveAdapter
import org.scenariotools.iosmk.examples.rovercontroller.rover

val roverDriveInterfaceScenario = scenario("RoverDriveInterfaceScenario"){
    println("Starting $name")

    while(true){

        waitFor(SymbolicMessageEvent(DriveAdapter::setLRPower, ANY, ANY))

        val speedLeft = lastMessage()!!.parameters[0] as Int
        val speedRight = lastMessage()!!.parameters[1] as Int
//            println("LEFT  SPEED: $speedLeft")
//            println("RIGHT SPEED: $speedRight")

        rover.noReply("Rover,setLRPower($speedLeft,$speedRight)")

    }
}
