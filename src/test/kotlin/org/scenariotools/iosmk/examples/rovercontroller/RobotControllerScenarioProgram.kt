package org.scenariotools.iosmk.examples.rovercontroller

import org.scenariotools.iosmk.ANY
import org.scenariotools.iosmk.ScenarioProgram
import org.scenariotools.iosmk.SymbolicMessageEvent
import org.scenariotools.iosmk.examples.rovercontroller.activeScenarios.roverDriveInterfaceScenario
import org.scenariotools.iosmk.examples.rovercontroller.activeScenarios.roverTelemetryInterfaceScenario
import org.scenariotools.iosmk.examples.rovercontroller.guarantees.*
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*

val rover = SocketCommunicator()
val ref = SocketCommunicator()
val IP = "127.0.0.1"

fun main (args:Array<String>) {

    Connector().connect(rover, ref, IP)

    val scenarioProgram = ScenarioProgram()
    scenarioProgram.addEnvironmentClass(Sensors::class)
    scenarioProgram.addGuaranteeScenario(predictLeaderPos)
    scenarioProgram.addGuaranteeScenario(move)
    scenarioProgram.addGuaranteeScenario(turn)
    scenarioProgram.addGuaranteeScenario(setLRPower)
    scenarioProgram.addAssumptionScenario(roverTelemetryInterfaceScenario)
    scenarioProgram.addAssumptionScenario(roverDriveInterfaceScenario)

    scenarioProgram.run()

}

class Connector{

    fun connect(rover: SocketCommunicator, ref: SocketCommunicator, IP: String) {
        var refPort = 0
        var roverPort = 0
        try {
            //------ Load config
            val conf =
                    try{
                        println("reading config file outside Jar: ./Settings/config.txt")
                        readFileOutsideJar("./Settings/config.txt")
                    }catch (e:FileNotFoundException){
                        println("could not read config file outside Jar, reading config file in Jar")
                        readFileInJar("/org/scenariotools/iosmk/examples/rovercontroller/config.txt")
                    }

            val conIt = conf.iterator()
            while (conIt.hasNext()) {
                val a = conIt.next()
                if (a.contains("=") == true) {
                    val b = a.substring(0, a.indexOf('='))
                    if (b == "controlPort")
                        roverPort = Integer.parseInt(a.substring(a.indexOf('=') + 1))
                    else if (b == "observationPort")
                        refPort = Integer.parseInt(a.substring(a.indexOf('=') + 1))
                }
            }
            rover.connectToServer(IP, roverPort)
            ref.connectToServer(IP, refPort)
        } catch (e: Exception) {
        }

    }

    private fun readFileOutsideJar(fileName: String): List<String> {
        val inputStream = FileInputStream(fileName)
        return readFileFromInputStream(inputStream)
    }

    private fun readFileInJar(fileName: String): List<String> {
        val inputStream = this.javaClass.getResourceAsStream(fileName)
        return readFileFromInputStream(inputStream)
    }

    private fun readFileFromInputStream(inputStream: InputStream): List<String> {
        val data = ArrayList<String>()
        val scanner: Scanner
        scanner = Scanner(inputStream)

        while (scanner.hasNext()) {
            data.add(scanner.next())
            println(data.last())
        }
        scanner.close()
        return data
    }

}
