package org.scenariotools.iosmk.examples.rovercontroller

// Author: Michal Pasternak
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket

class SocketCommunicator {

    private var `in`: BufferedReader? = null
    private var out: PrintWriter? = null
    private var socket: Socket? = null

    @Throws(IOException::class)
    fun connectToServer(IP: String, port: Int) {

        // Make connection and initialize streams
        socket = Socket(IP, port)
        `in` = BufferedReader(
                InputStreamReader(socket!!.getInputStream()))
        out = PrintWriter(socket!!.getOutputStream(), true)
        println("connected")
    }

    // Send command for sending a message through the socket and returning the reply
    fun send(message: String): String? {
        var reply: String? = null
        if (out == null) {
            println("fail - socket error")
        }
        out!!.println(message)
        try {
            //System.out.println("trying to read");
            reply = `in`!!.readLine()
            //System.out.println("Read reply: " + reply);
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return reply
    }

    // expecting no reply.
    fun noReply(message: String) {
        out!!.println(message)
    }

    // Closes the connection on the socket
    fun close() {
        try {
            socket!!.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }


    }
}
