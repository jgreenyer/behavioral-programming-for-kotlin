package org.scenariotools.iosmk.examples.rovercontroller

import org.scenariotools.iosmk.ScenarioProgram
import org.scenariotools.iosmk.examples.rovercontroller.activeScenarios.roverTelemetryTestScenario
import org.scenariotools.iosmk.examples.rovercontroller.guarantees.*
import kotlin.system.measureTimeMillis

fun main (args:Array<String>) {


    val scenarioProgram = ScenarioProgram()
    scenarioProgram.addEnvironmentClass(Sensors::class)
    scenarioProgram.addGuaranteeScenario(predictLeaderPos)
    scenarioProgram.addGuaranteeScenario(move)
    scenarioProgram.addGuaranteeScenario(turn)
    scenarioProgram.addGuaranteeScenario(setLRPower)
    scenarioProgram.addAssumptionScenario(roverTelemetryTestScenario)

    val timeElapsed = measureTimeMillis {
        scenarioProgram.run()
    }

    println("Time required: $timeElapsed ms")


}
