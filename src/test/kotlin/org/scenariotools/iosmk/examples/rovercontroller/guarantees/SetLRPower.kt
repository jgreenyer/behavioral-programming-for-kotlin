package org.scenariotools.iosmk.examples.rovercontroller.guarantees

import org.scenariotools.iosmk.*
import org.scenariotools.iosmk.examples.rovercontroller.DriveAdapter
import org.scenariotools.iosmk.examples.rovercontroller.DriveCmd
import org.scenariotools.iosmk.examples.rovercontroller.FollowerCtrl


val setLRPower = scenario("SetLRPower", SymbolicMessageEvent(FollowerCtrl, DriveCmd, DriveCmd::move, ANY)) {

        val mk = lastMessage()!!.parameters[0] as MoveKind

        waitFor(FollowerCtrl to DriveCmd send DriveCmd::turn param ANY)

        val tk = lastMessage()!!.parameters[0] as TurnKind

        val (speedLeft, speedRight) = computeLRSpeeds(mk, tk)

        request(DriveCmd to DriveAdapter send DriveAdapter::setLRPower param speedLeft param speedRight)
    }

private fun computeLRSpeeds(mk : MoveKind, tk : TurnKind) : Pair<Int, Int>{
    var speedLeft = 0
    var speedRight = 0

    when (mk){
        MoveKind.STOP -> when (tk) {
            TurnKind.LEFT -> {
                speedLeft = -50
                speedRight = 50
            }
            TurnKind.RIGHT -> {
                speedLeft = 50
                speedRight = -50
            }
            TurnKind.NONE -> {
                speedLeft = 0
                speedRight = 0
            }
        }
        MoveKind.FWD -> when (tk) {
            TurnKind.LEFT -> {
                speedLeft = -50
                speedRight = 100
            }
            TurnKind.RIGHT -> {
                speedLeft = 100
                speedRight = -50
            }
            TurnKind.NONE -> {
                speedLeft = 100
                speedRight = 100
            }
        }
        MoveKind.BWD -> when (tk) {
            TurnKind.LEFT -> {
                speedLeft = -100
                speedRight = 50
            }
            TurnKind.RIGHT -> {
                speedLeft = 50
                speedRight = -100
            }
            TurnKind.NONE -> {
                speedLeft = -100
                speedRight = -100
            }
        }
    }

//    println("speedLeft: $speedLeft")
//    println("speedRight: $speedRight")

    return Pair<Int, Int>(speedLeft, speedRight)
}



