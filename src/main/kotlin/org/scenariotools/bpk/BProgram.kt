/**
 * Copyright (c) 2018 Joel Greenyer.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Joel Greenyer
 */
package org.scenariotools.bpk

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*


data class NameToBThreadMain(val name:String, val bThreadMain: suspend BThread.() -> Unit)
fun bthread(name:String, bThreadMain: suspend BThread.() -> Unit) = NameToBThreadMain(name, bThreadMain)

open class AbstractBSyncMessage()
data class TerminatingBThreadMessage(val sender: BThread) : AbstractBSyncMessage()
data class SuspendMessage(val sender: BThread, val blockedEvents: IEventSet) : AbstractBSyncMessage()
data class AddBThreadsMessage(val sender: BThread, var bThreadsToBeAdded: Set<NameToBThreadMain>) : AbstractBSyncMessage()
data class BSyncMessage(val sender: BThread, val requestedEvents: IConcreteEventSet, val waitedForEvents: IEventSet, val blockedEvents: IEventSet) : AbstractBSyncMessage()


object RESUME

/**
 * A BProgram starts and coordinates the execution of BThreads.
 *
 * It repeatedly performs the following steps:
 * 1. Waits to receive bSync messages from all busy BThreads
 * 2. Event selection: select a requested event that is not blocked by any other BThread.
 * 3. Send the selected event to all BThreads that requested or waited for the selected event
 */
open class BProgram(vararg val initialBThreads: NameToBThreadMain) {

    val bSyncChannel: Channel<AbstractBSyncMessage> = Channel()
    val bResumeChannel: Channel<BThread> = Channel()
    protected var numberOfBUSYBThreads = 0
    protected val activeBThreadsToLastBSychMessage: MutableMap<BThread, BSyncMessage> = mutableMapOf()
    protected val suspendedBThreadsToBlockedEvents: MutableMap<BThread, IEventSet> = mutableMapOf()


    protected fun BThread.runBThread(bThreadMain : suspend BThread.() -> Unit) = coroutineScope.launch {
        bThreadMain()
        bSyncChannel.send(TerminatingBThreadMessage(this@runBThread))
    }

    protected fun startNewBThread(bThread : NameToBThreadMain, coroutineScope : CoroutineScope){
        BThread(bSyncChannel, bResumeChannel, coroutineScope, bThread.name).runBThread(bThread.bThreadMain)
        //BThread(bSyncChannel, bResumeChannel, GlobalScope, bThread.name).runBThread(bThread.bThreadMain)

        // number of BUSY BThreads (BThreads from which the BProgram awaits the next bSync message).
        // Is incremented when a BThread is added, decremented when BThread terminates or waits
        // BProgram ends when numberOfActiveBThreads == 0
        numberOfBUSYBThreads++
    }

    open fun run() = runBlocking {
        for (bt in initialBThreads) {
            startNewBThread(bt, this)
        }

        // main loop
        while (numberOfBUSYBThreads > 0 || !suspendedBThreadsToBlockedEvents.isEmpty()) {
            eventSelectionAndNotification(GlobalScope)
            resumeSuspendedBThreadsIfPossible()
        }
    }

    private suspend fun resumeSuspendedBThreadsIfPossible() {
        while (!suspendedBThreadsToBlockedEvents.isEmpty() && !bResumeChannel.isEmpty){
            val resumedBThread = bResumeChannel.receive()
            numberOfBUSYBThreads++
            suspendedBThreadsToBlockedEvents.remove(resumedBThread)
        }
    }

    private suspend fun eventSelectionAndNotification(mainCoroutineScope : CoroutineScope) {
        if (numberOfBUSYBThreads > 0) {

            receiveBSynchMessages(mainCoroutineScope)

            val selectableEvents = calculateSelectableEvents()
            val selectedEvent = selectEvent(selectableEvents)

            if (selectedEvent != null) {
                eventSelected(selectedEvent)
                notifyActiveBThreads(selectedEvent)
            }
        }
    }


    /**
     * Can be extended to change event selection strategy
     */
    open fun selectEvent(selectableEvents: IConcreteEventSet): Event? =
        if (!selectableEvents.isEmpty())
            selectableEvents.first()
        else
            null


    /**
     * Can be extended to monitor, log, etc. selected events
     */
    open fun eventSelected(selectedEvent: Event) {}


    private suspend fun receiveBSynchMessages(mainCoroutineScope : CoroutineScope) {
        while (numberOfBUSYBThreads > 0) {
            val abstractBSyncMessage = bSyncChannel.receive()
            when (abstractBSyncMessage) {
                is BSyncMessage -> {
                    activeBThreadsToLastBSychMessage.put(abstractBSyncMessage.sender, abstractBSyncMessage)
                    numberOfBUSYBThreads--
                }
                is SuspendMessage -> {
                    suspendedBThreadsToBlockedEvents.put(abstractBSyncMessage.sender, abstractBSyncMessage.blockedEvents)
                    numberOfBUSYBThreads--
                }
                is TerminatingBThreadMessage -> {
                    if(suspendedBThreadsToBlockedEvents.containsKey(abstractBSyncMessage.sender))
                        suspendedBThreadsToBlockedEvents.remove(abstractBSyncMessage.sender)
                    else {
                        activeBThreadsToLastBSychMessage.remove(abstractBSyncMessage.sender)
                        numberOfBUSYBThreads--
                    }
                }
                is AddBThreadsMessage -> {
                    for (bt in abstractBSyncMessage.bThreadsToBeAdded) {
                        startNewBThread(bt, mainCoroutineScope)
                    }
                }
                else -> handleBSynchMessage(abstractBSyncMessage)
            }
        }
    }

    open fun handleBSynchMessage(abstractBSyncMessage: AbstractBSyncMessage){

    }

    private fun calculateSelectableEvents(): IConcreteEventSet {
        val allSelectableEvents = MutableConcreteEventSet()
        val allBlockedEventSets = MutableNonConcreteEventSet()

        var thereAreRequestedEvents = false

        for (bThreadToEventsMapEntry in activeBThreadsToLastBSychMessage) {
            val be = bThreadToEventsMapEntry.value.blockedEvents
            if (be == NOEVENTS || be is MutableNonConcreteEventSet && be.size == 1 && be.iterator().next() == NOEVENTS) continue

            allBlockedEventSets.add(bThreadToEventsMapEntry.value.blockedEvents)
            //println("${c++}======= BLOCKED: ${bThreadToEventsMapEntry.value.blockedEvents} BY ${bThreadToEventsMapEntry.key.name} $numberOfBUSYBThreads ${activeBThreadsToLastBSychMessage.size}")
        }

        for (suspendedBThreadsToBlockedEventsEntry in suspendedBThreadsToBlockedEvents) {
            allBlockedEventSets.add(suspendedBThreadsToBlockedEventsEntry.value)
        }

        for (bThreadToEventsMapEntry in activeBThreadsToLastBSychMessage) {
            for (requestedEvent in bThreadToEventsMapEntry.value.requestedEvents) {
                thereAreRequestedEvents = true
                if (!allBlockedEventSets.contains(requestedEvent)) {
                    allSelectableEvents.add(requestedEvent)
                }else{
                    //println("======= BLOCKED: $requestedEvent")
                }
            }
        }

        if (thereAreRequestedEvents && allSelectableEvents.isEmpty())
            throw RuntimeException("BProgram is stuck scenario BTreads requesting events that are all blocked by other BThreads.")

        return allSelectableEvents
    }

    private suspend fun notifyActiveBThreads(selectedEvent: Event) {
        val bThreadToEventsMapEntryIterator = activeBThreadsToLastBSychMessage.iterator()

        while (bThreadToEventsMapEntryIterator.hasNext()) {
            val bThreadToEventsMapEntry = bThreadToEventsMapEntryIterator.next()
            if (bThreadToEventsMapEntry.value.waitedForEvents.contains(selectedEvent)
                    || bThreadToEventsMapEntry.value.requestedEvents.contains(selectedEvent)) {
                bThreadToEventsMapEntry.key.notifyOfEventChannel.send(selectedEvent)
                bThreadToEventsMapEntryIterator.remove()
                numberOfBUSYBThreads++
            }
        }

    }

}