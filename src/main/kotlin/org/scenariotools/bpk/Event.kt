package org.scenariotools.bpk

open class Event() : IConcreteEventSet {
    override val size: Int
        get() = 1

    override fun containsAll(elements: Collection<Event>): Boolean {
        if (elements.size == 0)
            return true
        else if (elements.size == 1)
            return contains(elements.first())
        else
            return false
    }

    override fun isEmpty(): Boolean {
        return false
    }

    override fun iterator(): Iterator<Event> {
        return SingleEventIterator(this)
    }

    override fun contains(element: Event): Boolean {
        return this.equals(element)
    }

    private class SingleEventIterator(private val e : Event) : Iterator<Event>{

        var hasNext = true

        override fun hasNext(): Boolean {
            return hasNext
        }

        override fun next(): Event {
            hasNext = false
            return e
        }

    }
}
