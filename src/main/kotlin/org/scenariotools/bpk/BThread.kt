package org.scenariotools.bpk

import kotlinx.coroutines.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlin.coroutines.coroutineContext

open class BThread (val bSyncChannel : Channel<AbstractBSyncMessage>, val bResumeChannel : Channel<BThread>, val coroutineScope: CoroutineScope, val name:String=""){

    val notifyOfEventChannel : Channel<Event> = Channel()

    private var suspended = false

//    fun runBThread() = coroutineScope.launch {
//        bThreadMain()
//        bSyncChannel.send(TerminatingBThreadMessage(this@BThread))
//    }

    suspend fun terminate(cause : String? = null){
        if (suspended)
            bResume()
        bSyncChannel.send(TerminatingBThreadMessage(this@BThread))
        throw CancellationException(cause)
    }


    suspend fun bSync(requestedEvents: IConcreteEventSet, waitedForEvents : IEventSet, blockedEvents : IEventSet) : Event {
        val bSyncMessage = BSyncMessage(this@BThread, requestedEvents, waitedForEvents, blockedEvents)
        bSyncChannel.send(bSyncMessage)
        val receivedEvent = notifyOfEventChannel.receive()
        return receivedEvent
    }

    suspend fun addBThreads(bThreadMains : Set<NameToBThreadMain>) {
//        val bThreadsToBeAdded = mutableSetOf<BThread>()
//        for (bThreadMain in bThreadMains){
//            bThreadsToBeAdded.add(BThread(bSyncChannel, bResumeChannel, coroutineScope, bThreadMain))
//        }
        val addBThreadMessage = AddBThreadsMessage(this@BThread, bThreadMains)
        bSyncChannel.send(addBThreadMessage)
    }

    suspend fun addBThread(name : String, bThreadMain : suspend BThread.() -> Unit) {
        addBThreads(setOf(NameToBThreadMain(name, bThreadMain)))
    }

    suspend fun bSuspend(milliseconds : Long, blockedEvents: IEventSet) {
        bSuspend(blockedEvents)
        delay(milliseconds)
        bResume()
    }

    suspend fun bSuspend(milliseconds : Int, blockedEvents: IEventSet, logMessage : String) {
        bSuspend(blockedEvents)
        val deciseconds = milliseconds.div(100)
        val remainder = milliseconds.rem(100)
        repeat (deciseconds) {
            delay(100)
            println("... suspended " + it +"00 ms ($name): $logMessage (" + it + "00 of " + deciseconds + "00), " + ((it * 100) / deciseconds) + "%")
        }
        delay(remainder.toLong())
        bResume()
    }

    suspend fun bSuspend() {
        bSuspend(NOEVENTS)
    }

    suspend fun bSuspend(blockedEvents: IEventSet) {
        bSyncChannel.send(SuspendMessage(this@BThread, blockedEvents))
        suspended = true
    }

    suspend fun bSuspend(milliseconds : Int, logMessage : String) {
        bSuspend(milliseconds, NOEVENTS, logMessage)
    }

    suspend fun bSuspend(milliseconds : Int) {
        bSuspend(milliseconds, NOEVENTS, "")
    }

    suspend fun bResume() {
        if (!suspended)
            throw RuntimeException("BThread $name is not suspended, thus cannot be resumed.")
        bResumeChannel.send(this)
        suspended = false
    }

    suspend open fun requestAndBlock(requestedEvents: IConcreteEventSet, blockedEvents : IEventSet) : Event {
        return bSync(requestedEvents, NOEVENTS, blockedEvents)
    }

    suspend open fun request(requestedEvents: IConcreteEventSet) : Event {
        return requestAndBlock(requestedEvents, NOEVENTS)
    }

    suspend open fun waitForAndBlock(waitedForEvents : IEventSet, blockedEvents : IEventSet) : Event {
        return bSync(NOEVENTS, waitedForEvents, blockedEvents)
    }

    suspend open fun waitFor(waitedForEvents : IEventSet) : Event {
        return waitForAndBlock(waitedForEvents, NOEVENTS)
    }

	override fun toString() : String = name

}