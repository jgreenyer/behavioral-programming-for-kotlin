package org.scenariotools.iosmk

import org.scenariotools.bpk.Event
import kotlin.reflect.KFunction

interface IFunctionCallEvent{
    fun callFunction()
}

interface IFunctionCallWithResultEvent : IFunctionCallEvent{
    fun result() : Any?
}

class MessageEvent<T>(val sender:Any, val receiver:Any, val messageType:KFunction<T>, vararg val parameters : Any) : Event(), IFunctionCallWithResultEvent {

    override fun hashCode() =
            (31 * sender.hashCode()
                    + 31 * receiver.hashCode()
                    + 31 * messageType.hashCode()
                    + parameters.sumBy { 31 * it.hashCode() })

    override fun equals(other : Any?): Boolean {

        fun parameterValuesEqual(otherMessageEvent : MessageEvent<*>) : Boolean{
            if (parameters.size != otherMessageEvent.parameters.size)
                assert(false, {"There must not be different numbers of scenario"})
            for (i in 0..parameters.size-1)
                if (parameters.get(i) != otherMessageEvent.parameters.get(i)) return false
            return true
        }

        if (other is MessageEvent<*>)
            return signatureEquals(other, sender, receiver, messageType)
                    && parameterValuesEqual(other)
        else
            return false
    }


    override fun toString(): String {
        val paramString = parameters.joinToString(separator = ", ", prefix = "(", postfix = ")")
        return sender.toString() + "->" + receiver.toString() + "." + messageType.name + paramString
    }

    private var functionCallResult : T? = null

    override fun callFunction() {
        try{
            if (null == receiver::class.objectInstance){
                functionCallResult = messageType.call(receiver, *parameters)
            }else{ //  class is an object declaration -> no receiver parameter!
                functionCallResult = messageType.call(*parameters)
            }
        }catch(e: Exception){
            println("Failed to reflectively invoke method call $messageType on object $receiver associated scenario message event ${this.toString()}, $e")
        }
    }

    override fun result() = functionCallResult

}


fun signatureEquals(messageEvent : MessageEvent<*>, sender: Any, receiver: Any, messageType : Any) : Boolean {
    if (messageType != messageEvent.messageType)
        return false
    if (sender != messageEvent.sender)
        return false
    if (receiver != messageEvent.receiver)
        return false
    return true
}

infix fun <T> Pair<Any,Any>.send(messageType:KFunction<T>) = MessageEvent<T>(this.first, this.second, messageType)
infix fun <T> Any.sendSelf(messageType:KFunction<T>) = MessageEvent<T>(this, this, messageType)
infix fun <T> MessageEvent<T>.param(parameter : Any) = MessageEvent<T>(sender, receiver, messageType, *this.parameters, parameter)
infix fun <T> MessageEvent<T>.params(parameters : Array<Any>) = MessageEvent<T>(sender, receiver, messageType, *this.parameters, *parameters)
