package org.scenariotools.iosmk

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.scenariotools.bpk.*
import java.lang.RuntimeException
import kotlin.reflect.KClass


data class AddScenariosMessage(val sender: Scenario, var scenariosToBeAdded: Set<NameToScenarioMain>, val scenarioKind: ScenarioKind, val currentEvent : MessageEvent<*>) : AbstractBSyncMessage()
data class InitEventSetToScenarioMain(val name : String, val eventSet : IEventSet, val scenarioMain : suspend Scenario.() -> Unit)
fun scenario(name:String, eventSet : IEventSet, scenarioMain: suspend Scenario.() -> Unit) = InitEventSetToScenarioMain(name, eventSet, scenarioMain)
data class NameToScenarioMain(val name : String, val scenarioMain : suspend Scenario.() -> Unit)
fun scenario(name: String, scenarioMain: suspend Scenario.() -> Unit) = NameToScenarioMain(name, scenarioMain)

class ScenarioProgram(
        val environmentClasses : MutableSet<KClass<*>> = mutableSetOf(),
        val activeAssumptionScenarios : MutableSet<Pair<String, suspend Scenario.() -> Unit>> = mutableSetOf(),
        val activeGuaranteeScenarios : MutableSet<Pair<String, suspend Scenario.() -> Unit>> = mutableSetOf(),
        val assumptionScenarios : MutableSet<InitEventSetToScenarioMain> = mutableSetOf(),
        val guaranteeScenarios : MutableSet<InitEventSetToScenarioMain> = mutableSetOf()
    ) : BProgram() {

    fun Scenario.run(scenarioMain : suspend Scenario.() -> Unit) = coroutineScope.launch {
        scenarioMain()
        bSyncChannel.send(TerminatingBThreadMessage(this@run))
    }

    fun startNewScenario(scenarioKind: ScenarioKind, name : String, scenarioMain : suspend Scenario.() -> Unit, eventThatInitializedScenario : MessageEvent<*>? = null){
        Scenario(bSyncChannel, bResumeChannel, GlobalScope, scenarioKind, name, eventThatInitializedScenario).run(scenarioMain)

        numberOfBUSYBThreads++
    }

    override fun run() {
        startNewScenario(ScenarioKind.GUARANTEE, "Initializer", initializerScenario)
        for (activeScenario in activeAssumptionScenarios){
            startNewScenario(ScenarioKind.ASSUMPTION, activeScenario.first, activeScenario.second)
        }
        for (activeScenario in activeGuaranteeScenarios){
            startNewScenario(ScenarioKind.GUARANTEE, activeScenario.first, activeScenario.second)
        }
        super.run()
    }

    val initializerScenario : suspend Scenario.() -> Unit = {
        while (true){
            val currentEvent = waitFor(ALLEVENTS)
            startScenariosWithEvent(assumptionScenarios, ScenarioKind.ASSUMPTION, currentEvent)
            startScenariosWithEvent(guaranteeScenarios, ScenarioKind.GUARANTEE, currentEvent)
        }
    }

    override fun selectEvent(selectableEvents: IConcreteEventSet): Event? {
        var selectedEvent= findFirstSystemEvent(selectableEvents)
        if (selectedEvent==null && !selectableEvents.isEmpty()) {
            selectedEvent = selectableEvents.first()
        }
        if (selectedEvent!=null ){
            //println("### EXECUTING $selectedEvent")
            if (selectedEvent is IFunctionCallEvent){
                selectedEvent.callFunction()
            }
        }
        return selectedEvent
    }

    private fun findFirstSystemEvent(selectableEvents: IConcreteEventSet) : Event?{
        for (event in selectableEvents){
            if (event is MessageEvent<*> && isSystemEvent(event)) return event
        }
        return null
    }

    private fun isSystemEvent(messageEvent: MessageEvent<*>) : Boolean {
        return !(messageEvent.sender::class in environmentClasses)
    }

    override fun handleBSynchMessage(abstractBSyncMessage: AbstractBSyncMessage) {
        when (abstractBSyncMessage) {
            is AddScenariosMessage -> {
                for (scenario in abstractBSyncMessage.scenariosToBeAdded) {
                    startNewScenario(abstractBSyncMessage.scenarioKind, scenario.name, scenario.scenarioMain, abstractBSyncMessage.currentEvent)
                }
            }
        }
    }

    fun addAssumptionScenario(initEventSetToScenarioMain : InitEventSetToScenarioMain){
        assumptionScenarios.add(initEventSetToScenarioMain)
    }
    fun addGuaranteeScenario(initEventSetToScenarioMain : InitEventSetToScenarioMain){
        guaranteeScenarios.add(initEventSetToScenarioMain)
    }
    fun addAssumptionScenario(name:String, eventSet: IEventSet, scenarioMain: suspend Scenario.() -> Unit){
        assumptionScenarios.add(InitEventSetToScenarioMain(name, eventSet, scenarioMain))
    }
    fun addGuaranteeScenario(name:String, eventSet: IEventSet, scenarioMain: suspend Scenario.() -> Unit){
        guaranteeScenarios.add(InitEventSetToScenarioMain(name, eventSet, scenarioMain))
    }
    fun addAssumptionScenario(name:String, scenarioMain: suspend Scenario.() -> Unit){
        activeAssumptionScenarios.add(name to scenarioMain)
    }
    fun addGuaranteeScenario(name:String, scenarioMain: suspend Scenario.() -> Unit){
        activeGuaranteeScenarios.add(name to scenarioMain)
    }
    fun addAssumptionScenario(nameToScenarioMain : NameToScenarioMain){
        activeAssumptionScenarios.add(nameToScenarioMain.name to nameToScenarioMain.scenarioMain)
    }
    fun addGuaranteeScenario(nameToScenarioMain : NameToScenarioMain){
        activeGuaranteeScenarios.add(nameToScenarioMain.name to nameToScenarioMain.scenarioMain)
    }
    fun addEnvironmentClass(kClass : KClass<*>){
        environmentClasses.add(kClass)
    }

}

class ViolationException(message:String = "") : RuntimeException(message)

