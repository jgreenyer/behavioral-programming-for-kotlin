package org.scenariotools.iosmk

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import org.scenariotools.bpk.*
import kotlin.reflect.KClass
import kotlin.reflect.KFunction

enum class ScenarioKind {
    ASSUMPTION, GUARANTEE
}

class Scenario(
        bSyncChannel : Channel<AbstractBSyncMessage>,
        bResumeChannel : Channel<BThread>,
        coroutineScope: CoroutineScope,
        val scenarioKind: ScenarioKind,
        name : String = "",
        val eventThatInitializedScenario : Event? = null) : BThread(bSyncChannel, bResumeChannel, coroutineScope, name){


    var lastEvent : Event? = null

    init{
        lastEvent = eventThatInitializedScenario
    }

    val interruptingEvents = MutableNonConcreteEventSet()
    val blockedEvents = MutableNonConcreteEventSet()

    @Suppress("UNCHECKED_CAST")
    suspend fun <T> request(messageEvent: MessageEvent<T>) : MessageEvent<T> {
        return requestAndBlock(messageEvent, NOEVENTS) as MessageEvent<T>
    }

    override suspend fun request(requestedEvents: IConcreteEventSet) : MessageEvent<*> {
        return requestAndBlock(requestedEvents, NOEVENTS)
    }

    override suspend fun requestAndBlock(requestedEvents: IConcreteEventSet, blockedEvents : IEventSet) : MessageEvent<*> {
        val allBlockedEvents = MutableNonConcreteEventSet()
        allBlockedEvents.addAll(this.blockedEvents)
        allBlockedEvents.add(blockedEvents)

        // todo: Implement assumption scenario semantics
        val event = bSync(requestedEvents, interruptingEvents, allBlockedEvents)  as MessageEvent<*>
        if (interruptingEvents.contains(event)) {
            //println("Scenario $name is interrupted by message event $event")
            terminate("Scenario $name interrupted by message event $event")
        }else {
            lastEvent = event
        }
        return event
    }

    override suspend fun waitForAndBlock(waitedForEvents : IEventSet, blockedEvents : IEventSet) : MessageEvent<*> {
        val allBlockedEvents = MutableNonConcreteEventSet()
        allBlockedEvents.addAll(this.blockedEvents)
        allBlockedEvents.add(blockedEvents)

        // todo: Implement assumption scenario semantics
        val allWaitedForEvents = MutableNonConcreteEventSet()
        allWaitedForEvents.add(waitedForEvents)
        allWaitedForEvents.addAll(interruptingEvents)
        val event = bSync(NOEVENTS, allWaitedForEvents, allBlockedEvents)  as MessageEvent<*>
        if (interruptingEvents.contains(event)) {
            //println("Scenario $name is interrupted by message event $event")
            terminate("Scenario $name is interrupted by message event $event")
        }else {
            lastEvent = event
        }
        return event

//        return waitForAndBlock(waitedForEvents, blockedEvents) as MessageEvent
    }

    suspend fun startScenarios(scenarios : Set<NameToScenarioMain>, scenarioKind: ScenarioKind, currentEvent : MessageEvent<*>) {
        val addScenariosMessage = AddScenariosMessage(this, scenarios, scenarioKind, currentEvent)
        bSyncChannel.send(addScenariosMessage)
    }

    suspend fun startScenario(name : String, scenarioMain : suspend Scenario.() -> Unit, scenarioKind: ScenarioKind, currentEvent : MessageEvent<*>) {
        startScenarios(setOf(NameToScenarioMain(name, scenarioMain)), scenarioKind, currentEvent)
    }

    suspend fun startScenariosWithEvent(initEventSetToScenarioMains : Set<InitEventSetToScenarioMain>, scenarioKind: ScenarioKind, currentEvent : MessageEvent<*>){
        for (initEventSetToScenarioMain in initEventSetToScenarioMains){
            if(initEventSetToScenarioMain.eventSet.contains(currentEvent)){
                startScenario(initEventSetToScenarioMain.name, initEventSetToScenarioMain.scenarioMain, scenarioKind, currentEvent)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun waitFor(waitedForEvents : IEventSet) : MessageEvent<*>{
        return waitForAndBlock(waitedForEvents, NOEVENTS)
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun <T> waitFor(messageEvent: MessageEvent<T>) : MessageEvent<T> {
        return waitForAndBlock(messageEvent, NOEVENTS) as MessageEvent<T>
    }

    fun lastMessage() : MessageEvent<*>?{
        if (lastEvent is MessageEvent<*>)
            return lastEvent as MessageEvent<*>
        else
            return null
    }

}