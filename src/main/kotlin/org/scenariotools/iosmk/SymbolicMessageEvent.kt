package org.scenariotools.iosmk

import org.scenariotools.bpk.Event
import org.scenariotools.bpk.IEventSet
import java.lang.UnsupportedOperationException
import kotlin.reflect.KClass
import kotlin.reflect.KFunction


class SymbolicMessageEvent(val sender:Any?, val receiver:Any?, val messageTypes: Collection<KFunction<*>>, vararg val parameters : Any) : IEventSet{


    constructor(sender : Any?, receiver: Any?, messageType: KFunction<*>, vararg parameters : Any) : this(sender, receiver, setOf(messageType), *parameters)

    constructor(messageTypes: Collection<KFunction<*>>, vararg parameters : Any) : this(null, null, messageTypes, *parameters)

    constructor(messageType: KFunction<*>, vararg parameters : Any) : this(null, null, setOf(messageType), *parameters)

    /**
     * Checks whether the given element is in the set of message events represented by this symbolic message event.
     *
     * The method returns true if
     * (1) the signature of the message event is compatible scenario that of the symbolic message event. This is the case when
     *     (a) message type of the message event is contained in the set of message types
     *     (b) the sender/receiver specified for this symbolic message event is null,
     *         or a class of which the sender/receiver of the message event is an instance,
     *         or the same object as the sender/receiver of the message event.
     * (2) the parameter of the message event are compatible scenario those of the symbolic message event.
     *     If this symbolic message has i parameters and the compared message event has j parameters,
     *     then if i == j, each message event parameter must be compatible scenario the symbolic event parameter counterpart.
     *     The message event parameter is compatible to a symbolic message event parameter if a concrete parameter value
     *     is specified by the symbolic message event, and the values are equal, if a set is specified by the symbolic
     *     message event, e.g., an integer range, and the message event parameter is an element of that set,
     *     or if the symbolic message event specifies the ANY value.
     *     If i < j, then the first i message event parameters must be unifiable to the i symbolic message event
     *     parameters, in their order, and all remaining message event parameters are ignored, i.e., it is equivalent to
     *     checking them against the ANY value.
     *     If i > j, the rationale is that the symbolic message event specifically requires more parameters, and,
     *     therefore, we return false in this case.
     */
    override fun contains(element: Event): Boolean {
        if (!(element is MessageEvent<*>))
            return false
        if (!signatureUnifyable(element))
            return false
        if (parameters.size > element.parameters.size) return false
        for (i in 0 until parameters.size){
            val thisParamVal = parameters[i]


            val otherParamVal = element.parameters[i]
            if (thisParamVal == otherParamVal)
                continue
            when (thisParamVal){
                ANY -> {}
                is Collection<*> -> if (!thisParamVal.contains(otherParamVal)) return false
                is IntRange ->
                    if (!(otherParamVal is Int) || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }
                is LongRange ->
                    if (!(otherParamVal is Long) || !thisParamVal.contains(otherParamVal)) {
                        return false
                    }
                else -> {
                        //parameter values not compatible
                        return false
                }
            }
        }
        return true
    }

    private fun signatureUnifyable(messageEvent: MessageEvent<*>): Boolean {
        if (!messageTypes.contains(messageEvent.messageType))
            return false

        if (!unifiable(sender, messageEvent.sender))
            return false
        if (!unifiable(receiver, messageEvent.receiver))
            return false

//        if(parameters.size != messageEvent.parameters.size)
//            return false

        return true
    }

    private fun unifiable(symbolicObj : Any?, obj : Any) : Boolean {
        if (symbolicObj == null || symbolicObj == ANY)
            return true
        else if (symbolicObj is KClass<*>){ // obj unbound, then types must be unifiable
            return (symbolicObj.isInstance(obj))
        } else { // obj bound, then bound objects must be equal
            return  (symbolicObj == obj)
        }
    }

    override fun isEmpty() = false

    override fun toString(): String {
        val paramString = parameters.joinToString(separator = ", ", prefix = "(", postfix = ")")
        val messageTypesString = messageTypes.map { it -> it.name }.joinToString(separator = ", ", prefix = "[", postfix = "]")
        return sender.toString() + "->" + receiver.toString() + "." + messageTypesString + paramString
    }

}

object ANY

object ANYMESSAGETYPE : Collection<KFunction<*>>{

    override val size = 1

    override fun contains(element: KFunction<*>) = true

    override fun containsAll(elements: Collection<KFunction<*>>) = true

    override fun isEmpty() = false

    override fun iterator(): Iterator<KFunction<*>> {
        return object : Iterator<KFunction<*>>{

            override fun hasNext(): Boolean {
                return false
            }

            override fun next(): KFunction<*> {
                throw UnsupportedOperationException()
            }
        }
    }
}

infix fun Pair<Any,Any>.sendSymbolic(messageType:KFunction<*>) = SymbolicMessageEvent(this.first, this.second, messageType)
infix fun Any.sendSelfSymbolic(messageType:KFunction<*>) = SymbolicMessageEvent(this, this, messageType)
infix fun SymbolicMessageEvent.param(parameter : Any) = SymbolicMessageEvent(sender, receiver, messageTypes, *this.parameters, parameter)
infix fun SymbolicMessageEvent.params(parameters : Array<Any>) = SymbolicMessageEvent(sender, receiver, messageTypes, *this.parameters, *parameters)

fun KFunction<*>.symbolicEvent() = SymbolicMessageEvent(this)